from tkinter.messagebox import *
from tkinter import *
import random
import sys
import datetime
import argparse

'''
Fonction propager elle gère la propagation du feu
'''
def propager (ligne, colonne): 
    voisin_vert = 0
    
    if ligne - 1 != -1 :
        if canvas.itemcget(list_complete[(ligne-1)*cols+colonne], "fill") == "gray" :
            voisin_vert = voisin_vert + 1
  
    if colonne - 1 != -1 :
        if canvas.itemcget(list_complete[ligne*cols+(colonne-1)], "fill") == "gray" :
            voisin_vert = voisin_vert + 1

    if colonne + 1 != cols :
        if canvas.itemcget(list_complete[ligne*cols+(colonne+1)], "fill") == "red" :
            voisin_vert = voisin_vert + 1

    if  ligne + 1 != rows :
        if canvas.itemcget(list_complete[(ligne+1)*cols+colonne], "fill") == "red" :
            voisin_vert = voisin_vert + 1
  
    if huit_voisins_vert == True :
        if ligne - 1 != -1 and colonne - 1 != -1:
            if canvas.itemcget(list_complete[(ligne-1)*cols+(colonne-1)], "fill") == "gray" :
                voisin_vert = voisin_vert + 1
        
        if ligne + 1 != rows and colonne - 1 != -1 :
            if canvas.itemcget(list_complete[(ligne+1)*cols+(colonne-1)], "fill") == "red" :
                voisin_vert = voisin_vert + 1
        
        if ligne + 1 != rows and colonne + 1 != cols:
            if canvas.itemcget(list_complete[(ligne+1)*cols+(colonne+1)], "fill") == "red" :
                voisin_vert = voisin_vert + 1

        if ligne - 1 != -1 and colonne + 1 != cols:
            if canvas.itemcget(list_complete[(ligne-1)*cols+(colonne+1)], "fill") == "gray" :
                voisin_vert = voisin_vert + 1

    return voisin_vert

'''
Fonction qui verifie si un voisin et en feu ou non
'''
def voisin_feu(ligne, colonne):
    crame = False
    voisin_vert = propager(ligne, colonne)
   
    if aux_feux_les_pompiers == True :
        if voisin_vert > 0:
            crame = True
    else :
        alea = random.randint(0,1)
        if (1-(1/(voisin_vert+1))) > alea:
            crame = True
    
    return crame

'''
Focntion qui declanche l'incendie
'''
def incendie():
    for ligne in range(rows):
        for colonne in range(cols):
            couleur = canvas.itemcget(list_complete[ligne*cols+colonne], "fill")
            crame = False
            if couleur == "green" :
                crame = voisin_feu(ligne, colonne)
                
            if  crame  and couleur == "green" or couleur != "white" and couleur != "green" :
               changer_etat_cell(ligne, colonne, crame)
    if pas_a_pas == False and crame == False :
        root.after(anim,incendie)
'''
Fonction pour jouer la simulation automatiquement
'''
def bouton_play():
    global pas_a_pas
    pas_a_pas = False
    incendie()
'''
Fonction pour jouer la simulation pas à pas
'''
def bouton_play_pas_a_pas():
    global pas_a_pas
    pas_a_pas = True
    incendie()
'''
L'incendie suis la loi de Moore
'''
def bouton_Moore():
    global huit_voisins_vert
    huit_voisins_vert = True
'''
L'incendie suis la loi de VonNeumann
'''
def bouton_Von_Neumann():
    global huit_voisins_vert
    huit_voisins_vert = False
'''
Le feu se propage normalement
'''
def bouton_feu():
    global aux_feux_les_pompiers
    aux_feux_les_pompiers = True
'''
La feu se propage selon un formule de probabilité
'''
def bouton_feu_proba():
    global aux_feux_les_pompiers
    aux_feux_les_pompiers = False

'''
Le clic qui declanche un feu sur un arbre
'''
def click_callback(event):
    if event.num == 1 or event.num == 3:
        x1 = (int)(event.x/taille_cells)
        y1 = (int)(event.y/taille_cells)
        if canvas.itemcget(list_complete[y1*cols+x1], "fill") == "green":
            canvas.itemconfig(list_complete[y1*cols+x1], fill='red')
        
'''
La fonction qui gere le changement d'état d'une cellule
'''
def changer_etat_cell(ligne, colonne, crame):
    couleur_actuelle = canvas.itemcget(list_complete[ligne*cols+colonne], "fill")
    
    if couleur_actuelle == "gray":
        nouvelle_couleur = "white"
    elif couleur_actuelle == "red":
        nouvelle_couleur = "gray"
    elif couleur_actuelle == "green" and crame:
        nouvelle_couleur = "red"
    elif couleur_actuelle == "green" and not crame:
        nouvelle_couleur = "green"
    
    if couleur_actuelle != nouvelle_couleur:
        canvas.itemconfig(list_complete[ligne*cols+colonne], fill=nouvelle_couleur)
'''
La fonction qui regénère un foret
'''
def bouton_reset():

            global list_complete
            list_complete = list()
            

            posx = 0
            posy = 0
            for ligne in range(rows):

                for colonne in range(cols):
                    
                    alea = random.randint(0, 100)
                    if alea <= afforestation * 100: 
                        etat = "green"
                    else:
                        etat = "white"

                    list_complete.append(canvas.create_rectangle( posx, posy, posx + taille_cells, posy + taille_cells, fill=etat))
                    posx = posx + taille_cells

                
                posy = posy + taille_cells
                posx = 0

'''
Main contient gestion de la fenètre des argument passé en ligne de commande et des widgets
'''
if __name__ == '__main__':
    
    root = Tk()
    root.title("Feux de Foret")

    print(len(sys.argv))
    if(len(sys.argv) < 6):
        showinfo(title="Error argument",message="argument par defaut appliqué")
        rows = 10
        cols = 20
        anim = 500
        taille_cells = 35
        afforestation = 0.6
    else:
        parser = argparse.ArgumentParser()

        parser.add_argument("rows", help="nombre de lignes",type=int)
        parser.add_argument("cols", help="nombre de colones",type=int)
        parser.add_argument("animation", help="durée de l'animation",type=float)
        parser.add_argument("taille_cells", help="taillle des cellules",type=int)
        parser.add_argument("afforestation", help="afforestation",type=float)

        args = parser.parse_args()

        rows = args.rows
        cols = args.cols
        anim = args.animation
        taille_cells = args.taille_cells
        afforestation = args.afforestation

    canvas = Canvas(root, width =cols * taille_cells, height = rows * taille_cells)
    canvas.grid(row=0,columnspan=4, padx=30, pady=30)
    bouton_reset()
    canvas.bind('<Button-1>', click_callback)
    canvas.bind('<Button-3>', click_callback)

    bouton_play_pas_a_pas=Button(root, text="play pas à pas", command=bouton_play_pas_a_pas)
    bouton_play_pas_a_pas.grid(row=1,column=2)

    bouton_play=Button(root, text="play", command=bouton_play)
    bouton_play.grid(row=2,column=2)

    label_loi = Label(root,text="Loi choisie")
    label_loi.grid(row=1,column=0)

    bouton_Moore=Button(root, text="Loi Moore", command=bouton_Moore)
    bouton_Moore.grid(row=2,column=0)

    bouton_Von_Neumann=Button(root, text="Loi Von Neumann", command=bouton_Von_Neumann)
    bouton_Von_Neumann.grid(row=3,column=0) 

    label_propagation = Label(root,text="Propagation choisie")
    label_propagation.grid(row=1,column=1)

    bouton_feu=Button(root, text="Règle n°1", command=bouton_feu)
    bouton_feu.grid(row=2,column=1)

    bouton_feu_proba=Button(root, text="Règle n°2", command=bouton_feu_proba)
    bouton_feu_proba.grid(row=3,column=1)

    bouton_reset=Button(root, text="reset", command=bouton_reset)
    bouton_reset.grid(row=3,column=2)

    root.rowconfigure(0, weight=1)
    root.rowconfigure(1, weight=1)
    root.rowconfigure(2, weight=1)
    root.rowconfigure(3, weight=1)
    root.columnconfigure(0, weight=1)
    root.columnconfigure(1, weight=1)
    root.columnconfigure(2, weight=1)

    root.mainloop()